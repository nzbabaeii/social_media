from typing import Any
from django import http
from django.shortcuts import render , redirect ,get_object_or_404
from django.views import View
from .forms import *
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import authenticate , login , logout
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import *
from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy


class RegisterView(View):
    form_class = RegistrationForm

    def dispatch(self, request , *args, **kwargs) :
        if request.user.is_authenticated:
            return redirect('home:home')
        return super().dispatch(request,*args ,**kwargs)
    
    def get(self , request):
        form =self.form_class()
        return render(request ,'accounts/register.html' ,{'form':form})
    
    def post(self, request):
        form=self.form_class(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            User.objects.create_user(data['username'],data['email'],data['password'])
            messages.success(request,'user creation success' ,'success')
            return redirect('home:home')
        return render(request , 'accounts/register.html' , {'form':form})
    


class LoginView(View):
    form_class =LoginForm
    template_name = 'accounts/login.html'

    def setup(self , request , *args , **kwargs):
        self.next=request.GET.get('next')
        return super().setup(request, *args, **kwargs)


    def dispatch(self, request , *args, **kwargs) :
        if request.user.is_authenticated:
            return redirect('home:home')
        return super().dispatch(request,*args ,**kwargs)
    

    def get(self , request):
        form = self.form_class()
        return render(request , self.template_name , {'form':form})
    
    def post(self , request):
        form=self.form_class(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = authenticate(request , username =data['username'] ,password = data['password'])
            if user is not None:
                login(request , user)
                messages.success(request , 'login successfully','success')
                if self.next:
                    return redirect(self.next)
                return redirect('home:home')
            messages.error(request ,'password or username is wrong' ,'warning')
        return render(request , self.template_name ,{'form':form})



class LogoutView( LoginRequiredMixin,View):
    def get(self , request):
        logout(request)
        messages.success(request ,'logout successfully','success')
        return redirect('home:home')
    

class UserProfileView(LoginRequiredMixin,View):
    template_name = 'accounts/profile.html'
    def get(self , request , user_id):
        now_following=False
        user = get_object_or_404(User,pk=user_id)
        posts =user.posts.all()
        relation=Relation.objects.filter(user_who_follower=request.user , user_who_following=user_id).exists()
        if relation :
            now_following=True
        return render(request , self.template_name , {'user':user ,'posts':posts ,'now_following':now_following})



class  UserPasswordResetView(auth_views.PasswordResetView):
    template_name='accounts/password_reset_form.html'
    success_url=reverse_lazy('accounts:password_reset_done')
    email_template_name='accounts/password_reset_email.html'


class UserPasswordResetDoneView(auth_views.PasswordChangeDoneView):
    template_name = 'accounts/password_reset_done.html'



class UserPasswordResetConfirmView(auth_views.PasswordResetConfirmView):
    template_name='accounts/password_reset_confirm.html'
    success_url =reverse_lazy('accounts:password_reset_complete')


class  UserPasswordResetCompleteView(auth_views.PasswordResetCompleteView):
    template_name='accounts/password_reset_complete.html'



class UserFollowView(LoginRequiredMixin,View):
    def get(self,request, *args ,**kwargs):
        user=User.objects.get(id=kwargs['user_id'])
        relation=Relation.objects.filter( user_who_follower=request.user , user_who_following=user)
        if relation.exists():
            messages.error(request,'you are following this user','success')
        else:
            Relation(user_who_follower=request.user , user_who_following=user).save()
            messages.success(request,'you start following this user','success')
        return redirect('accounts:Profile',user.id)




class UserUnfollowView(LoginRequiredMixin,View):
    def get(self,request, *args ,**kwargs):
        user=User.objects.get(id=kwargs['user_id'])
        relation=Relation.objects.filter(user_who_follower=request.user , user_who_following=user)
        if relation.exists():
            relation.delete()
            messages.success(request ,'unfollowing successfully','success')
        else:
            messages.error(request,'you are not following this user','danger')
        return redirect('accounts:Profile', user.id)




class EditProfileView(LoginRequiredMixin , View):
    form_class=EditProfileForm

    def get(self , request):
        form=self.form_class(instance=request.user.profile , initial={'email':request.user.email})
        return render(request,'accounts/Edit_profile.html' ,{'form':form})
    
    def post(self , request):
        form=self.form_class(request.POST , instance=request.user.profile)
        if form.is_valid():
            form.save()
            request.user.email=form.cleaned_data['email']
            request.user.save()
            messages.success(request,'profile was edited','success')
        return redirect('accounts:Profile' ,request.user.id)
