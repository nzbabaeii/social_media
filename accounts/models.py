from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from PIL import Image

class Post(models.Model):
    user=models.ForeignKey(User , on_delete=models.CASCADE , related_name='posts')
    body=models.TextField()
    slug=models.SlugField()
    created_date=models.DateTimeField(auto_now_add=True)
    updated_date=models.DateTimeField(auto_now=True)

    class Meta:
        ordering=('-created_date','body')


    def __str__(self):
        return f'{self.slug} - {self.updated_date}'
    

    def get_absolute_url(self):
        return reverse('home:post_detail' , args=(self.id , self.slug))
    
    def likes_count(self):
        return self.post_votes.count()
    
    def user_can_like(self , user):
        user_like=user.user_Votes.filter(post=self)
        if user_like.exists():
            return True
        return False
    

class Relation(models.Model):
    user_who_follower=models.ForeignKey(User , on_delete=models.CASCADE,related_name='followers')
    user_who_following=models.ForeignKey(User,on_delete=models.CASCADE, related_name='followings')
    created_date=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.user_who_follower} start following {self.user_who_following}' 
    


class Comment(models.Model):
    user = models.ForeignKey(User , on_delete=models.CASCADE ,related_name='user_comments')
    post = models.ForeignKey(Post , on_delete=models.CASCADE , related_name='post_comments' )
    reply=models.ForeignKey('comment',on_delete=models.CASCADE , related_name='reply_comments', blank=True , null=True)
    is_reply=models.BooleanField(default=False)
    body=models.TextField(max_length=450)
    created_date=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.user} - {self.body[:30]}"
    



class Vote(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE , related_name='user_Votes')
    post=models.ForeignKey(Post , on_delete=models.CASCADE , related_name='post_votes')

    def __str__(self):
        return f"{self.user} liked {self.post.slug}"
    


   
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio=models.CharField(max_length=200 , null=True , blank=True)
    link=models.URLField(null=True , blank=True)
   