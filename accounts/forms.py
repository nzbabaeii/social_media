from typing import Any, Dict
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from accounts.models import Profile

class RegistrationForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    email=forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control'}))
    password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}))
    password2=forms.CharField(label='confirm password',widget=forms.PasswordInput(attrs={'class':'form-control'}))


    def clean_email(self):
        email=self.cleaned_data['email']
        user =User.objects.filter(email=email).exists()
        if user :
            raise ValidationError('this user already exist ')
        return email
    
    def clean(self):
        data=super().clean()
        p1=data.get('password')
        p2= data.get('password2')
        
        if p1 and p2 and p1 != p2:
            raise ValidationError('passwords must match')
        

class LoginForm(forms.Form):
     username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
     password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}))


class EditProfileForm(forms.ModelForm):
    email=forms.EmailField()

    class Meta:
        model=Profile
        fields=('bio', 'link')



