from django.urls import path
from . import views


app_name='accounts'


urlpatterns =[
    path('register/' , views.RegisterView.as_view(), name='Register'),
    path('login/' , views.LoginView.as_view(), name='Login'),
    path('logout/' , views.LogoutView.as_view(), name='Logout'),
    path('profile/<int:user_id>/' , views.UserProfileView.as_view(), name='Profile'),
    path('password-reset/',  views.UserPasswordResetView.as_view(),name='reset_password'),
    path('password-reset/done/',views.UserPasswordResetDoneView.as_view() ,name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/', views.UserPasswordResetConfirmView.as_view(),name='password_reset_confirm'),
    path('password-reset-complete/',views.UserPasswordResetCompleteView.as_view(),name='password_reset_complete'),
    path('follow/<int:user_id>/' ,views.UserFollowView.as_view() , name='Follow'),
    path('unfollow/<int:user_id>/',views.UserUnfollowView.as_view(), name='Unfollow'),
    path('edit/' ,views.EditProfileView.as_view(), name='Edit_profile'),
   
]
    