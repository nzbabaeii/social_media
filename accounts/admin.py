from django.contrib import admin
from .models import *

class PostAdmin(admin.ModelAdmin):
    list_display=('user','slug','updated_date')
    search_fields =('slug','body')
    list_filter =('updated_date',)
    prepopulated_fields={'slug':('body',)}
    raw_id_fields=('user',)
     

admin.site.register(Post ,PostAdmin)

admin.site.register(Relation)



class CommentAdmin(admin.ModelAdmin):
    list_display=('user' , 'post', 'created_date', 'is_reply')
    raw_id_fields=('user','post','reply')

admin.site.register(Comment,CommentAdmin)

admin.site.register(Vote)




