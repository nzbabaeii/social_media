from typing import Any
from django import http
from django.http.response import HttpResponse
from django.shortcuts import render , redirect ,get_object_or_404
from django.views import View
from accounts.models import *
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from .forms import *
from django.utils.text import slugify 
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


class HomeView(View):
    form_class=PostSearchForm

    def get(self , request):
        posts=Post.objects.all()
        if request.GET.get('search'):
            posts=posts.filter(body__contains=request.GET['search'])
        return render(request , 'home/index.html' ,{'posts':posts ,'form':self.form_class})
    


class PostDetailView(View):
    form_class=CommentCreateForm
    form_reply_class = CommentReplyForm

    def setup(self , request , *args , **kwargs):
        self.post_instance = get_object_or_404(Post ,pk=kwargs['post_id'] , slug=kwargs['post_slug'])
        return super().setup(request, *args, **kwargs)


    def get(self , request,post_id , post_slug):
        comments = self.post_instance.post_comments.filter(is_reply=False)
        can_like=False
        if request.user.is_authenticated and self.post_instance.user_can_like(request.user):
            can_like=True
        return render(request , 'home/detail.html',{'post':self.post_instance ,'comments':comments ,                                
        'form':self.form_class ,'form_reply':self.form_reply_class ,'can_like':can_like})
    
    @method_decorator(login_required)
    def post(self , request ,*args ,**kwargs):
        form=self.form_class(request.POST)
        if form.is_valid():
            commentt=form.save(commit=False)
            commentt.user = request.user
            commentt.post = self.post_instance
            commentt.save()
            messages.success(request,'your comment submitted' ,'success')
            return redirect('home:post_detail',self.post_instance.id  , self.post_instance.slug)
    
class PostDeleteView(LoginRequiredMixin,View):
    def get(self , request , post_id ):
        post=get_object_or_404(Post , pk=post_id )
        if request.user.id == post.user.id:
            post.delete()
            messages.success(request , 'post deleted' ,'success')
        else:
            messages.error(request,'post cant deleted' , 'warning')
        return redirect('home:home')



class PostUpdateView(LoginRequiredMixin,View):
    form_class=PostUpdateForm 


    def setup(self, request, *args, **kwargs):
        self.post_instance = get_object_or_404(Post, pk=kwargs['post_id'])
        return super().setup(request,*args,**kwargs)
    


    def dispatch(self, request, *args , **kwargs) :
        post = Post.objects.get(pk=kwargs['post_id'])
        if not request.user.id == post.user.id :
            messages.error(request, 'not allowed' , 'danger')
            return redirect('home:home')
        return super().dispatch(request, *args, **kwargs)
    



    def get(self, request, *args, **kwargs):
        post =self.post_instance
        form=self.form_class(instance=post)
        return render(request ,'home/update.html',{'form':form})



    def post(self, request, *args, **kwargs):
        post=self.post_instance
        form=self.form_class(request.POST , instance=post)
        if form.is_valid():
            new_post=form.save(commit=False)
            new_post.slug=slugify(form.cleaned_data['body'][:30])
            new_post.save()
            messages.success(request ,'post is updated' , 'success')
            return redirect('home:post_detail',post.id , post.slug)




  
        
class PostCreateView(LoginRequiredMixin , View):
    form_class = PostUpdateForm

    def get(self , request,*args ,**kwargs):
        form = self.form_class
        return render(request,'home/create.html',{'form':form})
    
    def post(self , request,*args ,**kwargs):
        form=self.form_class(request.POST)
        if form.is_valid():
            new_post=form.save(commit=False)
            new_post.slug=slugify(form.cleaned_data['body'][:30])
            new_post.user = request.user
            new_post.save()
            messages.success(request ,'new post created','success')
            return redirect('home:post_detail',new_post.id ,new_post.slug)

class PostAddReplyView(LoginRequiredMixin , View):
    form_class = CommentReplyForm

    def post(self,request, *args , **kwargs):
        post=get_object_or_404(post, id=kwargs['post_id'])
        Comment=get_object_or_404(Comment,id=kwargs['comment_id'])
        form = self.form_class(request.POST)
        if form.is_valid():
            reply=form.save(commit=False)
            reply.user=request.user
            reply.post=post
            reply.reply=Comment
            reply.is_reply=True
            reply.save()
            messages.success(request ,'reply submitted','success')
        return redirect('home:post_detail',post.id , post.slug)
    

class LikeView(LoginRequiredMixin,View):
    def get(self , request ,post_id):
        post=get_object_or_404(Post , id=post_id)
        like=Vote.objects.filter(post=post , user=request.user)
        if like.exists():
            messages.error(request,'you liked this post','danger')
        else:
            Vote(post=post , user=request.user).save()
            messages.success(request, 'like successfully','success')
        return redirect('home:post_detail',post.id , post.slug)
    
